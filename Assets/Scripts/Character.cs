﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterData", menuName = "ScriptableObjects/CharacterDataObject", order = 1)]
public class Character : ScriptableObject
{
    public string charName;
    public string menu;
    public int money;
    public Sprite sprite;
    public Sprite happySprite;
    public Sprite iconSprite;
    public string normalReact;
    public string criticalFail;
    public string goodFeedback;
    public string badFeedback;
    public List<string> startDialogueList = new List<string>(3);
}