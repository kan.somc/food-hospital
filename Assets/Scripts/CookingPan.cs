﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CookingPan : MonoBehaviour, IDropHandler, IDragHandler, IEndDragHandler, IBeginDragHandler, IClearable
{
    public static CookingPan Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        if (Instance != null) return;
        Instance = this;
    }

    [SerializeField] private Transform dropPoint;
    [SerializeField] private bool isHeating;
    [SerializeField] private bool disableDragOnAccepted;
    [SerializeField] private GameObject oilAnimObject;
    [SerializeField] private GameObject sauceAnimObject;
    [SerializeField] private GameObject sugarAnimObject;
    
    [Header("Extra Ingredients")]
    [SerializeField] private GameObject oilPrefab;
    [SerializeField] private GameObject normalSaucePrefab;
    [SerializeField] private GameObject lowSaltSaucePrefab;
    [SerializeField] private GameObject sugarPrefab;

    public List<Ingredient> items = new List<Ingredient>();
    private int oldSiblingIndex;
    private Vector3 startDragPos;
    private static readonly int Pour = Animator.StringToHash("pour");

    private void Start()
    {
        if (dropPoint) return;
        dropPoint = gameObject.transform.GetChild(0).transform;
    }

    private void Update()
    {
        if (!isHeating) return;
        foreach (var item in items)
        {
            item.IncreaseTime();
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Stove.Instance.ToggleFlame(true);
        startDragPos = transform.position;
        startDragPos.z = 0;
        oldSiblingIndex = transform.GetSiblingIndex();
        transform.SetAsLastSibling();
        gameObject.GetComponent<Image>().raycastTarget = false;
        AudioPlayer.Instance.PlaySfx(SFX.ObjectPickUp);
    }

    public List<GameObject> DropOnTrashCan()
    {
        var trashList = items.Select(item => item.gameObject).ToList();
        items.Clear();
        return trashList;
    }

    public void OnDrag(PointerEventData eventData)
    {
        var mousePos = Camera.main.ScreenToWorldPoint(eventData.position);
        mousePos.z = -5;
        transform.position = mousePos;
    }

    public void OnDrop(PointerEventData eventData)
    {
        var ingredient = eventData.pointerDrag.GetComponent<Ingredient>();
        if (!ingredient || ingredient.onPanSpriteList.Count <= 0) return;
        if (GetAcceptableItem(ingredient))
        {
            AcceptItem(ingredient);
            AudioPlayer.Instance.PlaySfx(SFX.ObjectPlace);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        gameObject.GetComponent<Image>().raycastTarget = true;
        transform.DOMove(startDragPos, 0.1f);
        transform.SetSiblingIndex(oldSiblingIndex);
    }

    public void FillOil()
    {
        var ingredient = oilPrefab.GetComponent<Ingredient>();
        if (!GetAcceptableItem(ingredient)) return;
        var oilGameObject = Instantiate(oilPrefab, dropPoint);
        oilGameObject.transform.position = dropPoint.position;
        items.Add(oilGameObject.GetComponent<Ingredient>());
        var oilImage = oilGameObject.GetComponent<Image>();
        oilImage.raycastTarget = false;
        oilImage.DOColor(Color.white, 0.25f).SetDelay(1).SetEase(Ease.OutSine);
        oilAnimObject.GetComponent<Animator>().SetTrigger(Pour);
        AudioPlayer.Instance.PlaySfx(SFX.OilDrop);
    }

    public void FillNormalSauce()
    {
        if (items.Count <= 0) return;
        if (items.Exists(ingredient => ingredient.name.Contains("Sauce"))) return;
        if (!items.Exists(ingredient => ingredient.name.Contains("Pork") || ingredient.name.Contains("Fish"))) return;
        
        var sauceGameObject = Instantiate(normalSaucePrefab, dropPoint);
        sauceGameObject.transform.position = dropPoint.position;
        items.Add(sauceGameObject.GetComponent<Ingredient>());
        var sauceImage = sauceGameObject.GetComponent<Image>();
        sauceImage.raycastTarget = false;
        sauceImage.DOColor(Color.white, 0.25f).SetDelay(1).SetEase(Ease.OutSine);
        sauceAnimObject.GetComponent<Animator>().SetTrigger(Pour);
        AudioPlayer.Instance.PlaySfx(SFX.SauceDrop);
    }
    
    public void FillLowSaltSauce()
    {
        if (items.Count <= 0) return;
        if (items.Exists(ingredient => ingredient.name.Contains("Sauce"))) return;
        if (!items.Exists(ingredient => ingredient.name.Contains("Pork") || ingredient.name.Contains("Fish"))) return;
        
        var sauceGameObject = Instantiate(lowSaltSaucePrefab, dropPoint);
        sauceGameObject.transform.position = dropPoint.position;
        items.Add(sauceGameObject.GetComponent<Ingredient>());
        var sauceImage = sauceGameObject.GetComponent<Image>();
        sauceImage.raycastTarget = false;
        sauceImage.DOColor(Color.white, 0.25f).SetDelay(1).SetEase(Ease.OutSine);
        sauceAnimObject.GetComponent<Animator>().SetTrigger(Pour);
        AudioPlayer.Instance.PlaySfx(SFX.SauceDrop);
    }
    
    public void FillSugar()
    {
        if (items.Count <= 0) return;
        if (items.Exists(ingredient => ingredient.name.Contains("Sugar"))) return;
        if (!items.Exists(ingredient => ingredient.name.Contains("Pork") || ingredient.name.Contains("Fish"))) return;
        var sugarGameObject = Instantiate(sugarPrefab, dropPoint);
        sugarGameObject.transform.position = dropPoint.position;
        items.Add(sugarGameObject.GetComponent<Ingredient>());
        var sugarImage = sugarGameObject.GetComponent<Image>();
        sugarImage.raycastTarget = false;
        sugarAnimObject.GetComponent<Animator>().SetTrigger(Pour);
        AudioPlayer.Instance.PlaySfx(SFX.SugarDrop);
    }

    private bool GetAcceptableItem(Ingredient ingredient)
    {
        var mustHaveList = ingredient.mustHaveIngredientList;
        var conflictList = ingredient.conflictIngredientList;
        var passedMustHave = true;
        var hasConflict = false;
        if (ingredient.mustHaveIngredientList.Count > 0)
        {
            passedMustHave = mustHaveList.TrueForAll
                (mustHave => items.Exists(item => item.name.Contains(mustHave)));
        }
        if (ingredient.conflictIngredientList.Count > 0)
        {
            hasConflict = conflictList.Exists
                (conflict => items.Exists(item => item.name.Contains(conflict)));
        }
        return passedMustHave && !hasConflict;
    }

    private void AcceptItem(Ingredient ingredient)
    {
        ingredient.GetComponent<Draggable>().DropAccepted(disableDragOnAccepted);
        ingredient.DropOnPan();
        ingredient.transform.position = dropPoint.transform.position;
        ingredient.transform.SetParent(dropPoint);
        items.Add(ingredient);
    }

    public void Heating(bool heating)
    {
        isHeating = heating;
    }
}