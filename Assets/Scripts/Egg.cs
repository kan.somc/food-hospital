﻿using UnityEngine.EventSystems;

public class Egg : Ingredient
{
    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        AudioPlayer.Instance.PlaySfx(SFX.EggPickUp);
    }
}