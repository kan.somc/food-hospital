using Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI commentText;
    [SerializeField] private Image imageIcon;
    [SerializeField] private Image imageLike;
    [SerializeField] private Sprite likeSprite;
    [SerializeField] private Sprite dislikeSprite;
    public Feedback feedback;

    private void Start()
    {
        imageIcon.sprite = feedback.characterSprite;
        imageLike.sprite = feedback.status == DishStatus.Perfect ? likeSprite : dislikeSprite;
        commentText.text = feedback.commentText;
    }

}
