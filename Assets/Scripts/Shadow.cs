using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Shadow : MonoBehaviour,IBeginDragHandler,IEndDragHandler
{
    public List<Image> shadows = new List<Image>();
    
    public void OnBeginDrag(PointerEventData eventData)
    {
        foreach (var shadow in shadows)
        {
            shadow.color = Color.clear;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        foreach (var shadow in shadows)
        {
            shadow.DOColor(new Color(0,0,0,0.4f),0.1f).SetDelay(0.1f);
        }
    }
}
