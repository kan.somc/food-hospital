using System;
using System.Collections.Generic;
using UnityEngine;

public class IngredientDispenser : MonoBehaviour
{
    [SerializeField] private GameObject ingredient;
    [SerializeField] private int minAmount;
    [SerializeField] private List<Transform> dispensePoints = new List<Transform>();

    private int currentDispensePoints;
    private List<GameObject> ingredientList = new List<GameObject>();

    private void Update()
    {
        if (ingredientList.Count < minAmount)
        {
            Dispense();
        }
    }

    private void Dispense()
    {
        var ingredientGameObject = Instantiate(ingredient, transform);
        ingredientGameObject.transform.position = dispensePoints[currentDispensePoints].position;
        ingredientList.Add(ingredientGameObject);
        currentDispensePoints++;
        if (currentDispensePoints >= dispensePoints.Count) currentDispensePoints = 0;
    }
}
