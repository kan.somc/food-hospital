using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Controllers
{
    public enum GameState
    {
        Store = 0,
        Cooking = 1,
        EndDay = 2,
    }

    public class GameController : MonoBehaviour
    {
        public static GameController Instance { get; private set; }

        [Header("Canvas")] [SerializeField] private GameObject storeCanvas;
        [SerializeField] private GameObject cookingCanvas;
        [SerializeField] private GameObject endDayCanvas;

        [Header("UI Cashier")] [SerializeField]
        private TextMeshProUGUI moneyText;

        [Header("Customer")] [SerializeField] private int currentDialogOptions;
        [SerializeField] private Transform charSpawnPoint;
        [SerializeField] private Transform charActivePoint;
        [SerializeField] private Transform charEndPoint;
        [SerializeField] private Image currentCharImage;
        private int currentCustomerCount;
        private List<Character> currentDataList;

        [Header("Dialogue")] [SerializeField] private RectTransform dialoguePanel;
        [SerializeField] private TextMeshProUGUI dialogueText;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private Button dialogueButton;
        [SerializeField] private Image fadeImage;
        [SerializeField] private Transform hiddenPanelPos;
        [SerializeField] private Transform activePanelPos;

        [Header("Feedback")] public List<GameObject> feedbacks = new List<GameObject>();
        public Button nextButton;
        public GameObject feedbackPrefab;
        public Transform contentTrans;

        [Header("Day List")] public List<Character> day1 = new List<Character>();
        public List<Character> day2 = new List<Character>();
        public List<Character> day3 = new List<Character>();
        public List<Character> day4 = new List<Character>();
        public List<Character> day5 = new List<Character>();

        private int currentDayCount;
        private int money = -30000;
        private Dictionary<int, List<Character>> dataList = new Dictionary<int, List<Character>>();

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (Instance != null) return;
            Instance = this;
            PopulateData();
        }

        private void Start()
        {
            fadeImage.DOFade(0, 2).SetEase(Ease.InOutQuad)
                .OnComplete(() =>
                {
                    fadeImage.raycastTarget = false;
                    currentDayCount = PlayerPrefs.GetInt("dayCount", 1);
                    currentDataList = dataList[currentDayCount];
                    NextCustomer();
                });
            moneyText.DOText(money.ToString(), 0.5f);
            UpdateMoney(PlayerPrefs.GetInt("money", money));
            nextButton.onClick.RemoveAllListeners();
            nextButton.onClick.AddListener(OnPressNextDay);
        }

        private void PopulateData()
        {
            dataList.Add(1, day1);
            dataList.Add(2, day2);
            dataList.Add(3, day3);
            dataList.Add(4, day4);
            dataList.Add(5, day5);
        }

        private void NextDialogue()
        {
            var dialogueList = currentDataList[currentCustomerCount].startDialogueList;
            if (currentDialogOptions + 1 < dialogueList.Count)
            {
                currentDialogOptions++;
                dialogueText.SetText("");
                dialogueText.DOText("", 0);
                dialogueText.DOText(dialogueList[currentDialogOptions], 1);
                AudioPlayer.Instance.PlaySfx(SFX.Talking);
            }

            if (currentDialogOptions + 1 < dialogueList.Count) return;
            SetCookButton();
        }

        private void StartCooking()
        {
            CookingController.Instance.currentRecipeName = currentDataList[currentCustomerCount].menu;
            SwitchState(GameState.Cooking);
        }

        public void Deliver(DishStatus status)
        {
            SwitchState(GameState.Store);
            var customer = currentDataList[currentCustomerCount];
            var feedbackItem = Instantiate(feedbackPrefab, contentTrans, false);
            feedbackItem.GetComponent<FeedbackItem>().feedback = new Feedback
            {
                status = status,
                characterSprite = customer.iconSprite,
                commentText = GetComment(customer, status)
            };
            feedbacks.Add(feedbackItem);
            dialogueText.text = "";
            currentCharImage.sprite = customer.happySprite;
            dialogueText.DOText(customer.normalReact, 2).SetDelay(2);
            dialogueButton.onClick.RemoveAllListeners();
            dialogueButton.onClick.AddListener(() =>
            {
                NextCustomer();
                if (customer.money == 0) return;
                UpdateMoney(money += customer.money);
                AudioPlayer.Instance.PlaySfx(SFX.MoneyDing);
            });
            currentCustomerCount++;
        }

        private void UpdateMoney(int amount)
        {
            money = amount;
            moneyText.DOText(money.ToString(), 1);
        }

        private string GetComment(Character character, DishStatus status)
        {
            return status switch
            {
                DishStatus.Critical => character.criticalFail,
                DishStatus.Vital => character.criticalFail,
                DishStatus.Perfect => character.goodFeedback,
                DishStatus.NotPerfect => character.badFeedback,
                _ => ""
            };
        }

        private void OnPressNextDay()
        {
            if (feedbacks.Exists(feedback =>
                    feedback.GetComponent<FeedbackItem>().feedback.status == DishStatus.Vital))
            {
                Fader.Instance.FadeTo("Failed");
                return;
            }

            if (currentDayCount == 5)
            {
                Fader.Instance.FadeTo("Completed");
                return;
            }

            var nextDayCount = currentDayCount + 1;
            PlayerPrefs.SetInt("money", money);
            PlayerPrefs.SetInt("dayCount", nextDayCount);
            Fader.Instance.FadeTo("Calendar");
        }

        private void EndDay()
        {
            DOTween.To(x => x += Time.deltaTime, 0, 0, 0)
                .SetDelay(1).OnComplete(() => SwitchState(GameState.EndDay));
        }

        private void SwitchState(GameState state)
        {
            fadeImage.raycastTarget = true;
            fadeImage.DOFade(1, 2).SetEase(Ease.InOutQuad)
                .OnComplete(() =>
                {
                    switch (state)
                    {
                        case GameState.Cooking:
                            cookingCanvas.SetActive(true);
                            storeCanvas.SetActive(false);
                            endDayCanvas.SetActive(false);
                            AudioPlayer.Instance.ChangeBGM(BGM.Gameplay);
                            break;
                        case GameState.Store:
                            cookingCanvas.SetActive(false);
                            storeCanvas.SetActive(true);
                            endDayCanvas.SetActive(false);
                            AudioPlayer.Instance.ChangeBGM(BGM.Gameplay);
                            break;
                        case GameState.EndDay:
                            cookingCanvas.SetActive(false);
                            storeCanvas.SetActive(false);
                            endDayCanvas.SetActive(true);
                            AudioPlayer.Instance.ChangeBGM(BGM.Feedback);
                            break;
                    }
                    fadeImage.DOFade(0, 2).SetEase(Ease.InOutQuad)
                        .OnComplete(() => fadeImage.raycastTarget = false);
                });
        }

        private void NextCustomer()
        {
            if (currentCustomerCount >= currentDataList.Count)
            {
                EndDay();
            }
            else
            {
                currentDialogOptions = 0;
                var characterData = currentDataList[currentCustomerCount];
                SetNextButton();
                nameText.text = characterData.charName;
                var nextSequence = DOTween.Sequence();
                nextSequence.Append(dialoguePanel.DOMove(hiddenPanelPos.position, 0.5f));
                nextSequence.Append(currentCharImage.transform.DOMove(charEndPoint.position, 1f)
                    .SetEase(Ease.InSine)
                    .OnComplete(() =>
                    {
                        currentCharImage.transform.position = charSpawnPoint.position;
                        currentCharImage.sprite = characterData.sprite;
                    }));
                nextSequence.Append(currentCharImage.transform.DOMove(charActivePoint.position, 2)
                    .SetEase(Ease.OutSine));
                nextSequence.Append(dialogueText.DOText("", 0));
                nextSequence.Append(dialoguePanel.DOMove(activePanelPos.position, 0.5f));
                nextSequence.Append(
                    dialogueText.DOText(currentDataList[currentCustomerCount].startDialogueList[currentDialogOptions],
                        1).OnStart(() => AudioPlayer.Instance.PlaySfx(SFX.Talking)));
            }
        }

        private void SetNextButton()
        {
            dialogueButton.onClick.RemoveAllListeners();
            dialogueButton.onClick.AddListener(NextDialogue);
        }

        private void SetCookButton()
        {
            dialogueButton.onClick.RemoveAllListeners();
            dialogueButton.onClick.AddListener(StartCooking);
        }
    }

    public class Feedback
    {
        public Sprite characterSprite;
        public string commentText;
        public DishStatus status;
    }
}