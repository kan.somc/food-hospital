using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private VideoPlayer vp;
    [SerializeField] private Button startButton;
    [SerializeField] private Button knowledgeButton;
    private TextMeshProUGUI buttonText;

    private void Awake()
    {
        PlayerPrefs.SetInt("dayCount",1);
        PlayerPrefs.SetInt("money",-30000);
    }

    private void Start()
    {
        vp.url = System.IO.Path.Combine (Application.streamingAssetsPath,"Video/MainMenuVideo.mp4");
        vp.loopPointReached += OnLoopPointReached;
        vp.Play();
        Invoke(nameof(MenuAppear),5);
    }

    private void MenuAppear()
    {
        vp.Pause();
        startButton.transform.DOScale(1, 0.25f).SetEase(Ease.OutSine)
            .OnComplete(EnableStart);
        knowledgeButton.transform.DOScale(1, 0.25f).SetEase(Ease.OutSine);
    }

    private void EnableStart()
    {
        startButton.onClick.AddListener(() =>
        {
            startButton.transform.DOScale(0,0.1f).SetEase(Ease.InExpo)
                .OnComplete(()=>startButton.gameObject.SetActive(false));
            startButton.enabled = false;
            knowledgeButton.transform.DOScale(0,0.1f).SetEase(Ease.InExpo)
                .OnComplete(()=>knowledgeButton.gameObject.SetActive(false));
            knowledgeButton.enabled = false;
            vp.Play();
        });
        knowledgeButton.onClick.AddListener(() =>
        {
            Application.OpenURL("https://www.siphhospital.com/th/news/article/share/food-for-kidney");
        });
    }

    private void OnLoopPointReached(VideoPlayer player)
    {
        Fader.Instance.FadeTo("Calendar");
    }

}