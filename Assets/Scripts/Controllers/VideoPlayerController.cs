using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerController : MonoBehaviour
{
    [SerializeField] private VideoPlayer vp;
    [SerializeField] private string clipName;
    [SerializeField] private string sceneToLoad = "Gameplay";
    
    private void Start()
    {
        if (!vp) vp = FindObjectOfType<VideoPlayer>();
        vp.url = GetUrl();
        vp.Play();
        vp.loopPointReached += Gameplay;
    }

    private string GetUrl()
    {
        if (!string.IsNullOrEmpty(clipName))
            return System.IO.Path.Combine(Application.streamingAssetsPath, $"Video/{clipName}.mp4");
        
        var dayCount = PlayerPrefs.GetInt("dayCount", 1);
        return System.IO.Path.Combine(Application.streamingAssetsPath, $"Video/Day{dayCount}.mp4");
    }

    private void Gameplay(VideoPlayer source)
    {
        Fader.Instance.FadeTo(sceneToLoad);
    }
}
