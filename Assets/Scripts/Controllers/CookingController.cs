using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Controllers
{
    public enum DishStatus
    {
        Perfect,
        NotPerfect,
        Critical,
        Vital,
    }

    public class CookingController : MonoBehaviour
    {
        public static CookingController Instance { get; private set; }

        private Recipe CurrentRecipe => RecipeIngredientList[currentRecipeName];
        public string currentRecipeName;
        public List<string> donenessStringList = new List<string> { "ดิบ", "สุก", "ไหม้" };

        [Header("Confirm Order")] public GameObject confirmOrder;
        public TextMeshProUGUI menuText;
        public TextMeshProUGUI ingredientText;
        public Button buttonServe;
        public Button buttonCancelServe;

        [Header("Recipe")] [SerializeField] private GameObject recipeBook;
        [SerializeField] private Image recipeImage;
        [SerializeField] private int currentPage;
        [SerializeField] private List<Sprite> pageList;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button previousButton;

        [Header("Order Slip")] [SerializeField]
        private GameObject orderSlipPanel;

        [SerializeField] private TextMeshProUGUI menuSlipText;

        public static Dictionary<string, Recipe> RecipeIngredientList = new Dictionary<string, Recipe>()
        {
            {
                "PorkCabbage", new Recipe("Oil", "PorkMinced", "Rice", "Cabbage", "NormalSauce", "Sugar")
            },
            {
                "PorkCabbageLowSalt",
                new Recipe("Oil", "PorkMinced", "Rice", "Cabbage", "LowSaltSauce", "Sugar", true, "NormalSauce")
            },
            {
                "PorkNoCabbage", new Recipe("Oil", "PorkMinced", "Rice", "", "NormalSauce", "Sugar", false, "Cabbage")
            },
            {
                "FishSteak", new Recipe("", "Fish", "", "", "NormalSauce", "")
            },
            {
                "FishRice", new Recipe("Oil", "FishChopped", "Rice", "", "NormalSauce", "Sugar")
            },
            {
                "PorkBasil", new Recipe("Oil", "PorkMinced", "Rice", "Basil", "NormalSauce", "Sugar")
            },
            {
                "PorkBasilLowSugar", new Recipe("Oil", "PorkMinced", "Rice", "Basil", "NormalSauce", "", true, "Sugar")
            },
            {
                "FriedEggYolk", new Recipe("Oil", "EggYolk", "Rice", "", "", "")
            },
            {
                "FriedEggWhite", new Recipe("Oil", "EggWhite", "Rice", "", "", "", false, "EggYolk")
            },
        };

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (Instance != null) return;
            Instance = this;
        }

        private void Start()
        {
            buttonCancelServe.onClick.AddListener(CloseConfirmOrder);
            buttonServe.onClick.AddListener(Serve);
            nextButton.onClick.AddListener(NextRecipe);
            previousButton.onClick.AddListener(PreviousRecipe);
        }

        public void OpenConfirmOrder(List<Ingredient> ingredients)
        {
            confirmOrder.gameObject.SetActive(true);
            menuText.text = GetMenu();
            ingredientText.text = GetText(ingredients);
            AudioPlayer.Instance.PlaySfx(SFX.BookOpen);
        }

        private void CloseConfirmOrder()
        {
            confirmOrder.gameObject.SetActive(false);
            AudioPlayer.Instance.PlaySfx(SFX.BookOpen);
        }

        public void OpenSlip()
        {
            orderSlipPanel.gameObject.SetActive(true);
            menuSlipText.text = "- " + GetMenu();
            AudioPlayer.Instance.PlaySfx(SFX.BookOpen);
        }

        public void CloseSlip()
        {
            orderSlipPanel.gameObject.SetActive(false);
            AudioPlayer.Instance.PlaySfx(SFX.BookOpen);
        }

        public void OpenRecipe()
        {
            recipeBook.gameObject.SetActive(true);
            AudioPlayer.Instance.PlaySfx(SFX.BookOpen);
        }

        public void CloseRecipe()
        {
            recipeBook.gameObject.SetActive(false);
            AudioPlayer.Instance.PlaySfx(SFX.BookOpen);
        }

        private void NextRecipe()
        {
            currentPage++;
            if (currentPage >= pageList.Count)
                currentPage = 0;
            recipeImage.sprite = pageList[currentPage];
            AudioPlayer.Instance.PlaySfx(SFX.BookFlip);
        }

        private void PreviousRecipe()
        {
            currentPage--;
            if (currentPage <= 0)
                currentPage = pageList.Count - 1;
            recipeImage.sprite = pageList[currentPage];
            AudioPlayer.Instance.PlaySfx(SFX.BookFlip);
        }

        private void Serve()
        {
            GameController.Instance.Deliver(CheckValidDish(Dish.Instance.ingredients));
            AudioPlayer.Instance.PlaySfx(SFX.MenuButtonClick);
            CloseConfirmOrder();
        }

        private string GetMenu()
        {
            if (currentRecipeName == "") return "";
            return currentRecipeName switch
            {
                "PorkCabbage" => "ผัดผักหมู",
                "PorkCabbageLowSalt" => "ผัดผักผู้ป่วยไต",
                "PorkNoCabbage" => "ผัดผักหมูไม่ใส่ผัก",
                "FishSteak" => "สเต็กซาบะ",
                "FishRice" => "ข้าวหน้าซาบะย่างซีอิ๊ว",
                "PorkBasil" => "ผัดกะเพรา",
                "PorkBasilLowSugar" => "ผัดกะเพราไม่มีน้ำตาล",
                "FriedEggYolk" => "ข้าวไข่ดาว",
                "FriedEggWhite" => "ข้าวไข่ดาวขาวล้วน",
                _ => ""
            };
        }

        private DishStatus CheckValidDish(List<Ingredient> ingredients)
        {
            if (!string.IsNullOrEmpty(CurrentRecipe.Critical))
            {
                var isCritical = ingredients.Exists(ingredient => ingredient.name.Contains(CurrentRecipe.Critical));
                if (isCritical)
                {
                    return CurrentRecipe.Vital ? DishStatus.Vital : DishStatus.Critical;
                }
            }

            var score = 0;
            foreach (var ingredient in ingredients)
            {
                if (CheckIngredient(CurrentRecipe.Critical, ingredient.name))
                {
                    return DishStatus.Critical;
                }

                if (CheckIngredient(CurrentRecipe.Oil, ingredient.name))
                {
                    score++;
                }
                else if (CheckIngredient(CurrentRecipe.Protein, ingredient.name))
                {
                    if (ingredient.GetDonenessTime() != (int)Doneness.Cooked)
                        return DishStatus.NotPerfect;
                    score++;
                }
                else if (CheckIngredient(CurrentRecipe.Veg, ingredient.name))
                {
                    if (ingredient.GetDonenessTime() != (int)Doneness.Cooked)
                        return DishStatus.NotPerfect;
                    score++;
                }
                else if (CheckIngredient(CurrentRecipe.Rice, ingredient.name))
                {
                    score++;
                }
                else if (CheckIngredient(CurrentRecipe.Sauce, ingredient.name))
                {
                    score++;
                }
                else if (CheckIngredient(CurrentRecipe.Sugar, ingredient.name))
                {
                    score++;
                }
                else
                {
                    return DishStatus.NotPerfect;
                }
            }

            return score == CurrentRecipe.PerfectScore ? DishStatus.Perfect : DishStatus.NotPerfect;
        }

        private bool CheckIngredient(string recipe, string itemName)
        {
            return !string.IsNullOrEmpty(recipe) && itemName.Contains(recipe);
        }

        private string GetText(List<Ingredient> ingredients)
        {
            var text = "";
            foreach (var ingredient in ingredients)
            {
                switch (ingredient.name)
                {
                    case var pork when pork.Contains("Oil"):
                        text += "- น้ำมัน\n";
                        break;
                    case var pork when pork.Contains("Pork"):
                        text += "- หมู ";
                        text += $"({donenessStringList[ingredient.GetDonenessTime()]})\n";
                        break;
                    case var saba when saba.Contains("Fish"):
                        text += "- ปลาซาบะ ";
                        text += $"({donenessStringList[ingredient.GetDonenessTime()]})\n";
                        break;
                    case var egg when egg.Contains("EggWhite"):
                        text += "- ไข่ขาว ";
                        text += $"({donenessStringList[ingredient.GetDonenessTime()]})\n";
                        break;
                    case var egg when egg.Contains("EggYolk"):
                        text += "- ไข่ดาว ";
                        text += $"({donenessStringList[ingredient.GetDonenessTime()]})\n";
                        break;
                    case var cabbage when cabbage.Contains("Cabbage"):
                        text += "- ผักกาด ";
                        text += $"({donenessStringList[ingredient.GetDonenessTime()]})\n";
                        break;
                    case var basil when basil.Contains("Basil"):
                        text += "- ใบกะเพรา \n";
                        break;
                    case var rice when rice.Contains("Rice"):
                        text += "- ข้าวสวย \n";
                        break;
                    case var sauce when sauce.Contains("NormalSauce"):
                        text += "- ซอสปรุงรส \n";
                        break;
                    case var lowSaltSauce when lowSaltSauce.Contains("LowSaltSauce"):
                        text += "- ซอสปรุงรส (ลดเค็ม) \n";
                        break;
                    case var sugar when sugar.Contains("Sugar"):
                        text += "- น้ำตาล \n";
                        break;
                }
            }

            return text;
        }
    }

    public class Recipe
    {
        public string Oil;
        public string Protein;
        public string Rice;
        public string Veg;
        public string Sauce;
        public string Sugar;
        public string Critical;
        public bool Vital;
        public int PerfectScore;
        public List<string> recipeStringList = new List<string>();

        public Recipe(string oil, string protein, string rice, string veg, string sauce, string sugar,
            bool vital = false, string critical = "")
        {
            Oil = oil;
            Protein = protein;
            Rice = rice;
            Veg = veg;
            Sauce = sauce;
            Sugar = sugar;
            Critical = critical;
            Vital = vital;
            if (!string.IsNullOrEmpty(oil))
                recipeStringList.Add(oil);
            if (!string.IsNullOrEmpty(protein))
                recipeStringList.Add(protein);
            if (!string.IsNullOrEmpty(rice))
                recipeStringList.Add(rice);
            if (!string.IsNullOrEmpty(veg))
                recipeStringList.Add(veg);
            if (!string.IsNullOrEmpty(sauce))
                recipeStringList.Add(sauce);
            if (!string.IsNullOrEmpty(sugar))
                recipeStringList.Add(sugar);
            PerfectScore = recipeStringList.Count;
        }
    }
}