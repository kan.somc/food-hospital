using System;
using DG.Tweening;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    public static AudioPlayer Instance { get; private set; }

    public AudioSource bgmSource;
    public AudioSource sfxSource;

    [Header("BGM")] 
    [SerializeField] private AudioClip bgmMainMenu;
    [SerializeField] private AudioClip bgmGameplay;
    [SerializeField] private AudioClip bgmFeedback;

    [Header("MainMenu")] 
    [SerializeField] private AudioClip sfxStartGame;
    [SerializeField] private AudioClip sfxButtonClick;
    [SerializeField] private AudioClip moneyDing;

    [Header("Cooking Zone")] 
    [SerializeField] private AudioClip objectPlace;
    [SerializeField] private AudioClip objectPickUp;
    [SerializeField] private AudioClip eggSplit;
    [SerializeField] private AudioClip eggCrack;
    [SerializeField] private AudioClip eggPickUp;
    [SerializeField] private AudioClip ricePlace;
    [SerializeField] private AudioClip oilDrop;
    [SerializeField] private AudioClip sauceDrop;
    [SerializeField] private AudioClip sugarDrop;
    [SerializeField] private AudioClip trashDrop;
    [SerializeField] private AudioClip bellRing;
    [SerializeField] private AudioClip stoveKnobTurn;
    [SerializeField] private AudioClip bookOpen;
    [SerializeField] private AudioClip bookFlip;
    [SerializeField] private AudioClip knifeChop;

    [Header("NPC")] 
    [SerializeField] private AudioClip npcTalking;

    private float bgmStartVol; 

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        if (Instance != null) return;
        Instance = this;

        if (!bgmSource) bgmSource = GetComponent<AudioSource>();
        if (!sfxSource) sfxSource = bgmSource.transform.GetChild(0).GetComponent<AudioSource>();
    }

    private void Start()
    {
        bgmStartVol = bgmSource.volume;
        bgmSource.volume = 0;
        bgmSource.DOFade(bgmStartVol, 1f);
        sfxSource.DOFade(1f, 1f);
    }
    
    public void ChangeBGM(BGM bgm)
    {
        var sequence = DOTween.Sequence();
        sequence.Append(bgmSource.DOFade(0f, 1).OnComplete(() =>
        {
            bgmSource.clip = GetBGM(bgm);
            bgmSource.Play();
        }));
        sequence.Append(bgmSource.DOFade(0.5f, 1));
    }

    public void PlaySfx(SFX sfx)
    {
        sfxSource.PlayOneShot(GetSfx(sfx));
    }

    private AudioClip GetSfx(SFX sfx)
    {
        return sfx switch
        {
            SFX.MenuButtonClick => sfxButtonClick,
            SFX.MenuStartGame => sfxStartGame,
            SFX.ObjectPlace => objectPlace,
            SFX.ObjectPickUp => objectPickUp,
            SFX.EggSplit => eggSplit,
            SFX.EggCrack => eggCrack,
            SFX.EggPickUp => eggPickUp,
            SFX.RicePlace => ricePlace,
            SFX.OilDrop => oilDrop,
            SFX.SauceDrop => sauceDrop,
            SFX.SugarDrop => sugarDrop,
            SFX.TrashDrop => trashDrop,
            SFX.BellRing => bellRing,
            SFX.GasKnob => stoveKnobTurn,
            SFX.BookOpen => bookOpen,
            SFX.BookFlip => bookFlip,
            SFX.KnifeChop => knifeChop,
            SFX.Talking => npcTalking,
            SFX.MoneyDing => moneyDing,
            _ => throw new ArgumentOutOfRangeException(nameof(sfx), sfx, null)
        };
    }
    
    private AudioClip GetBGM(BGM bgm)
    {
        return bgm switch
        {
            BGM.MainMenu => bgmMainMenu,
            BGM.Gameplay => bgmGameplay,
            BGM.Feedback => bgmFeedback,
            _ => throw new ArgumentOutOfRangeException(nameof(bgm), bgm, null)
        };
    }


    public void OnChangeScene(float duration)
    {
        bgmSource.DOFade(0, duration);
        sfxSource.DOFade(0, duration);
    }

}

public enum SFX
{
    MenuStartGame,
    MenuButtonClick,
    ObjectPlace,
    ObjectPickUp,
    EggSplit,
    EggCrack,
    EggPickUp,
    RicePlace,
    OilDrop,
    SauceDrop,
    SugarDrop,
    TrashDrop,
    BellRing,
    GasKnob,
    BookOpen,
    BookFlip,
    KnifeChop,
    Talking,
    MoneyDing,
}

public enum BGM
{
    MainMenu,
    Gameplay,
    Feedback,
}