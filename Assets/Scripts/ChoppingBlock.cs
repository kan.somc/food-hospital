using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChoppingBlock : MonoBehaviour, IDropHandler
{
    public static ChoppingBlock Instance { get; private set; }

    [SerializeField] private Transform dropPoint;
    [SerializeField] private List<string> acceptableItems = new List<string>();
    [SerializeField] private bool disableDragOnAccepted;
    [SerializeField] private GameObject eggCracked;
    [SerializeField] private GameObject eggWhiteCracked;
    [HideInInspector] public GameObject currentItem;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        if (Instance != null) return;
        Instance = this;
    }

    private void Start()
    {
        if (dropPoint) return;
        dropPoint = gameObject.transform.GetChild(0).transform;
    }

    private void OnEnable()
    {
        currentItem = null;
    }

    public void OnDrop(PointerEventData eventData)
    {
        var draggable = eventData.pointerDrag.GetComponent<Draggable>();
        if (!draggable) return;
        if (currentItem)
        {
            if (draggable.name.Contains("Knife") && !currentItem.name.Contains("Egg"))
            {
                if (currentItem.GetComponent<Ingredient>().onCut != null)
                {
                    Chop(currentItem);
                    AudioPlayer.Instance.PlaySfx(SFX.KnifeChop);
                    return;
                }
            }
            if (draggable.name.Contains("Separator") && currentItem.name.Contains("Egg"))
            {
                Destroy(currentItem);
                draggable = Instantiate(eggWhiteCracked).GetComponent<Draggable>();
                AcceptDraggable(draggable);
                AudioPlayer.Instance.PlaySfx(SFX.EggSplit);
                return;
            }
        }

        if (currentItem) return;
        if (!acceptableItems.Any(itemName => draggable.name.Contains(itemName))) return;
        if (draggable.name.Contains("Egg"))
        {
            Destroy(draggable.gameObject);
            AudioPlayer.Instance.PlaySfx(SFX.EggCrack);
            draggable = Instantiate(eggCracked).GetComponent<Draggable>();
        }
        else
        {
            AudioPlayer.Instance.PlaySfx(SFX.ObjectPlace);
            draggable.DropAccepted(disableDragOnAccepted);
        }

        AcceptDraggable(draggable);
    }

    private void AcceptDraggable(Draggable draggable)
    {
        var trans = draggable.transform;
        trans.SetParent(dropPoint, false);
        trans.position = dropPoint.position;
        currentItem = draggable.gameObject;
    }

    private void Chop(GameObject item)
    {
        currentItem = null;
        var ingredient = item.GetComponent<Ingredient>();
        if (!item.GetComponent<Ingredient>() || !ingredient.onCut) return;
        var onCutItem = Instantiate(ingredient.onCut, dropPoint);
        onCutItem.transform.position = dropPoint.position;
        currentItem = onCutItem;
        Destroy(item.gameObject);
    }
}