using System.Collections.Generic;
using System.Linq;
using Controllers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Dish : MonoBehaviour, IDropHandler, IClearable
{
    public Transform dropPoint;
    [SerializeField] private GameObject ricePrefab;
    public List<Ingredient> ingredients = new List<Ingredient>();
    public static Dish Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        if (Instance != null) return;
        Instance = this;
    }
    
    private void OnEnable()
    {
        if (ingredients.Count <= 0) return;
        TrashCan.Instance.DropItems(ingredients.Select(ingredient => ingredient.gameObject).ToList());
        ingredients.Clear();
        for (int i = 0; i < dropPoint.childCount; i++)
        {
            Destroy(dropPoint.GetChild(i).gameObject);
        }
    }

    public List<GameObject> DropOnTrashCan()
    {
        var trashList = new List<GameObject>();
        for (var i = 0; i < dropPoint.childCount; i++)
        {
            trashList.Add(dropPoint.GetChild(i).gameObject);
        }
        ingredients.Clear();
        TrashCan.Instance.ClearItems();
        return trashList;
    }

    public void OnDrop(PointerEventData eventData)
    {
        eventData.pointerDrag.TryGetComponent(out CookingPan pan);
        if (!pan) return;
        if (ingredients.Count > 0 && ingredients.Exists(item => !item.name.Contains("Rice"))) return;
        if (pan.items.Exists(ingredient => ingredient.name.Contains("FishWhole")))
        {
            DropOnTrashCan();
        }
        if (pan.items.Exists(ingredient => ingredient.onDishSpriteList.Count > 0))
        {
            SpawnDish(pan);
        }
    }

    public void AddRice()
    {
        if (ingredients.Count > 0 && ingredients.Exists(ingredient => ingredient.name.Contains("Rice"))) return;
        var riceGameObject = Instantiate(ricePrefab, dropPoint, false);
        riceGameObject.transform.SetAsFirstSibling();
        ingredients.Add(riceGameObject.GetComponent<Ingredient>());
        AudioPlayer.Instance.PlaySfx(SFX.RicePlace);
    }

    public void PressBell()
    {
        if (ingredients.Count == 0) return;
        CookingController.Instance.OpenConfirmOrder(ingredients);
        AudioPlayer.Instance.PlaySfx(SFX.BellRing);
    }

    private void SpawnDish(CookingPan pan)
    {
        foreach (var item in pan.items)
        {
            ingredients.Add(item);
            if (item.onDishSpriteList.Count <= 0) continue;
            var sprite = item.onDishSpriteList[item.GetDonenessTime()];
            var itemGameObject = new GameObject();
            itemGameObject.transform.SetParent(dropPoint, false);
            var image = itemGameObject.AddComponent<Image>();
            image.raycastTarget = false;
            image.preserveAspect = true;
            image.sprite = sprite;
            image.SetNativeSize();
        }
        TrashCan.Instance.DropItems(pan.items.Select(items => items.gameObject).ToList());
        pan.items.Clear();
    }

}