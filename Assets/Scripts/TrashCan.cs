using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TrashCan : MonoBehaviour,IDropHandler
{
    public static TrashCan Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        if (Instance != null) return;
        Instance = this;
    }

    private void OnEnable()
    {
        ClearItems();
    }

    public void ClearItems()
    {
        for (var i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public void DropItems(List<GameObject> items)
    {
        foreach (var item in items)
        {
            item.transform.SetParent(transform, false);
            item.SetActive(false);
        }
    }
    
    public void OnDrop(PointerEventData eventData)
    {
        eventData.pointerDrag.TryGetComponent<IClearable>(out var clearable);
        var items = clearable?.DropOnTrashCan();
        if (items == null) return;
        DropItems(items);
        AudioPlayer.Instance.PlaySfx(SFX.TrashDrop);
    }
}
