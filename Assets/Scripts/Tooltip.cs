using DG.Tweening;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
    [Header("Tooltip")] public GameObject tooltipPanel;
    public bool mouseEnter;
    public float hoverTime = 0.25f;
    private float currentHoverTime;

    private void Update()
    {
        if (!tooltipPanel && !tooltipPanel.activeInHierarchy) return;
        if (mouseEnter) currentHoverTime += Time.deltaTime;
        CheckTooltip();
    }

    private void OnMouseEnter()
    {
        mouseEnter = true;
    }

    private void OnMouseExit()
    {
        currentHoverTime = 0;
        mouseEnter = false;
        if (tooltipPanel && tooltipPanel.activeInHierarchy)
            tooltipPanel.transform.DOScale(0, 0.25f).SetEase(Ease.InOutQuad)
                .OnComplete(() => tooltipPanel.SetActive(false));
    }

    private void CheckTooltip()
    {
        if (!(currentHoverTime >= hoverTime) || tooltipPanel.activeInHierarchy) return;
        tooltipPanel.transform.DOScale(1, 0.25f).SetEase(Ease.InOutQuad)
            .OnStart(() => tooltipPanel.SetActive(true));
    }
}