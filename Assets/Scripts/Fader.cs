using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    public static Fader Instance { get; private set; }
    public float fadeStartDelay = 1;
    public float fadeDuration;
    public Ease easeType;
    private Image image;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        if (Instance != null) return;
        Instance = this;
        image = GetComponent<Image>();
    }

    private void Start()
    {
        image.DOFade(0, fadeDuration).SetEase(easeType).SetDelay(fadeStartDelay).OnComplete(() => image.raycastTarget = false);
    }

    public void FadeTo(string scene)
    {
        image.raycastTarget = true;
        AudioPlayer.Instance.OnChangeScene(fadeDuration);
        image.DOFade(1, fadeDuration).SetEase(easeType).OnComplete(() => SceneManager.LoadScene(scene));
    }
}