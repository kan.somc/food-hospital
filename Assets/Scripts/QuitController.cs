using UnityEngine;
using UnityEngine.UI;

public class QuitController : MonoBehaviour
{
    public static QuitController Instance { get; private set; }

    [SerializeField] private GameObject quitCanvas;
    [SerializeField] private Button yesButton;
    [SerializeField] private Button noButton;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        if (Instance != null) return;
        Instance = this;
    }

    private void Start()
    {
        yesButton.onClick.RemoveAllListeners();
        noButton.onClick.RemoveAllListeners();
        yesButton.onClick.AddListener(OnYes);
        noButton.onClick.AddListener(OnNo);
    }

    public void OpenMenu()
    {
        quitCanvas.SetActive(true);
        AudioPlayer.Instance.PlaySfx(SFX.MenuButtonClick);
    }

    private void OnYes()
    {
        Fader.Instance.FadeTo("StartMenu");
        AudioPlayer.Instance.PlaySfx(SFX.MenuButtonClick);
    }

    private void OnNo()
    {
        quitCanvas.SetActive(false);
        AudioPlayer.Instance.PlaySfx(SFX.MenuButtonClick);
    }
}