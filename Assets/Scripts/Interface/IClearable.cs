﻿using System.Collections.Generic;
using UnityEngine;

public interface IClearable
{
    List<GameObject> DropOnTrashCan();
}