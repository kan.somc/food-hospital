using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Knife : Draggable
{
    [SerializeField] private Sprite idleSprite;
    [SerializeField] private Sprite holdingSprite;
    private Image knifeImage;

    private void Awake()
    {
        knifeImage = GetComponent<Image>();
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        knifeImage.sprite = holdingSprite;
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        knifeImage.sprite = idleSprite;
    }
}
