﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public enum Doneness
{
    Raw = 0,
    Cooked = 1,
    Burnt = 2
}

public class Ingredient : Draggable, IClearable
{
    [Header("Cooking")] public float minTimeCooked;
    public float maxTimeCooked;
    private float timeCooked;
    [Header("Sprites/GameObjects")] 
    public GameObject onCut;
    [Header("On Pan")] 
    public List<Sprite> onPanSpriteList = new List<Sprite>();
    [Header("On Dish")] 
    public List<Sprite> onDishSpriteList = new List<Sprite>();
    [Header("Accept/Decline Pan List")] public List<string> mustHaveIngredientList = new List<string>();
    public List<string> conflictIngredientList = new List<string>();

    private void CheckSprite()
    {
        if (maxTimeCooked == 0 && minTimeCooked == 0) return;
        if (timeCooked > maxTimeCooked)
        {
            Image.sprite = onPanSpriteList[(int)Doneness.Burnt];
        }
        else if (timeCooked > minTimeCooked)
        {
            Image.sprite = onPanSpriteList[(int)Doneness.Cooked];
        }
    }

    public void DropOnPan()
    {
        var currentItem = ChoppingBlock.Instance.currentItem;
        if (currentItem && currentItem.name == gameObject.name)
        {
            ChoppingBlock.Instance.currentItem = null;
        }

        Image.sprite = onPanSpriteList[(int)Doneness.Raw];
        Image.SetNativeSize();
    }

    public void IncreaseTime()
    {
        timeCooked += Time.deltaTime;
        CheckSprite();
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        AudioPlayer.Instance.PlaySfx(SFX.ObjectPickUp);
    }

    public int GetDonenessTime()
    {
        if (minTimeCooked == 0 && maxTimeCooked == 0) return (int)Doneness.Cooked;
        if (timeCooked < minTimeCooked)
        {
            return (int) Doneness.Raw;
        }
        if (timeCooked > maxTimeCooked)
        {
            return (int) Doneness.Burnt;
        }
        return (int) Doneness.Cooked;
    }

    public List<GameObject> DropOnTrashCan()
    {
        DropAccepted(true);
        if(ChoppingBlock.Instance.currentItem == gameObject)
        {
            ChoppingBlock.Instance.currentItem = null;
        }
        transform.DOScale(0, 0.1f);
        return new List<GameObject> {gameObject};
    }
}