﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Draggable : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    private bool canDrag = true;
    private bool accepted = false;
    protected Image Image;
    private Camera mainCam;
    private Vector3 startDragPos;
    private int originalOrder;
    private Color startColor;
    private Vector3 startScale;

    private void Start()
    {
        Image = GetComponent<Image>();
        mainCam = Camera.main;
        UpdateStartState();
    }

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        if (!canDrag) return;
        startDragPos = transform.position;
        startDragPos.z = 0;
        originalOrder = transform.GetSiblingIndex();
        transform.SetAsLastSibling();
        Image.raycastTarget = false;
        Image.DOColor(Color.white, 0.1f);
        transform.DOScale(1, 0.1f);
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        if (!canDrag) return;
        var mousePos = mainCam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = -5;
        transform.position = mousePos;
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        if (accepted)
        {
            accepted = false;
            UpdateStartState();
            return;
        }
        if (originalOrder != 0)
        {
            transform.SetSiblingIndex(originalOrder);
        }
        else
        {
            transform.SetAsLastSibling();
        }
        Image.DOColor(startColor, 0.1f);
        transform.DOScale(startScale, 0.1f);
        ReturnToPos();
    }

    private void UpdateStartState()
    {
        startColor = Image.color;
        startScale = transform.localScale;
    }

    private void ReturnToPos()
    {
        if (Image.raycastTarget) return;
        Image.raycastTarget = true;
        transform.DOMove(startDragPos, 0.1f);
    }

    public void DropAccepted(bool disableDrag)
    {
        accepted = true;
        canDrag = !disableDrag;
        Image.raycastTarget = !disableDrag;
    }

    public void DisableDrag()
    {
        Image.raycastTarget = false;
        canDrag = false;
    }
}