using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class Stove : MonoBehaviour,IPointerClickHandler
{
    public static Stove Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        if (Instance != null) return;
        Instance = this;
    }
    
    [SerializeField] private CookingPan pan;
    [SerializeField] private Transform fire;
    [SerializeField] private AudioSource audioSource;
    private readonly Vector3 offAngle = new Vector3(0f, 0f, 0f);
    private readonly Vector3 onAngle = new Vector3(0f, 0f, 90f);
    private bool isHeating;

    private void ToggleFlame()
    {
        isHeating = !isHeating;
        pan.Heating(isHeating);
        audioSource.enabled = isHeating;
        fire.DOScale(isHeating ? 1 : 0, 0.2f).SetEase(Ease.InOutExpo);
        transform.DORotate(isHeating ? onAngle : offAngle, 0.2f).SetEase(Ease.OutExpo);
        AudioPlayer.Instance.PlaySfx(SFX.GasKnob);
    }
    
    public void ToggleFlame(bool off)
    {
        if (!isHeating) return;
        isHeating = false;
        pan.Heating(false);
        audioSource.enabled = false;
        fire.DOScale(0, 0.05f).SetEase(Ease.InOutExpo);
        transform.DORotate(offAngle, 0.05f).SetEase(Ease.OutExpo);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ToggleFlame();
    }
}
